#!/usr/bin/python3
import os
import cpblUtilities as utils
import pystata as pst
#pst.paths['tex'] = './tex/'
pst.defaults['paths'].update(dict(output = pst.defaults['paths']['scratch'],
                                  tex = pst.defaults['paths']['scratch'],
                                  working = pst.defaults['paths']['scratch'],))
utils.defaults['paths'].update(pst.defaults['paths'])

os.system('mkdir -p {}'.format(pst.paths['tex']))
os.system('mkdir -p {}'.format(pst.paths['output']))
os.system('mkdir -p {}'.format(pst.paths['working']))
os.system('mkdir -p {}logs'.format(pst.paths['working']))
#os.system('mkdir -p output')
#os.system('mkdir -p output/tex')

# To use this, make sure to copy the config template here and fill it out fully.

def test_duplicate_regressors(latex):
    import os
    stataout=pst.doHeader+pst.stataLoad(#os.getcwd()+
        pst.defaults['paths']['bin']+'demo/sample_data_BES')
    dmodels=latex.str2models("""
    *name:Regressor listed twice
    reg aq1 aq5a aq6a aq5a aq7a [pw=aweightt],

    * Note: this string is working Stata code. However, it will get heavily preprocessed by pystata.
    *flag:clustering=wards
    reg aq1 aq5a aq6a aq7a [pw=aweightt], cluster(awardid)
    reg aq1 aq5a aq6a aq7a [pw=aweightt], beta
    *|
    *name:Special
    reg aq1 aq5a aq6a aq7a [pw=aweightt],
    """)
    stataout+=latex.regTable('simple demo',dmodels,returnModels=True,transposed='both')

    latex.regTable('simple demo',dmodels,showModels=[mm for mm in dmodels if mm['name'] in ['Special'] or mm.get('flags',{}).get('clustering','') in ['wards']],transposed='both', extraTexFileSuffix='-subset', skipStata=True)

    latex.closeAndCompile()
    pst.stataSystem(stataout,filename= pst.paths['working']+'demos')
    


def test_addCorrelationTable(latex):
    pst.tsv2dta(#pst.paths['bin']+'tests/'
                'sampledata1macronov2010.tsv')
    return ("""
        gzuse {}  ,clear
        """.format(#pst.paths['bin']+'tests/'
            'sampledata1macronov2010.dta.gz') + """
        """+latex.addCorrelationTable(
            'testMkCorr', 'gwp_beta*', ifClause=None) + """
            """)




pst.tsv2dta(pst.paths['bin']+'tests/' + 'sampledata1macronov2010.tsv',
            newdir= pst.paths['working'])

#from regressionsGallup import standardSubstitutions
pst.runBatchSet(
    'sVersion',
    'rVersion', [
        test_duplicate_regressors,
        test_addCorrelationTable
    ],
    dVersion='testingOnly-forCPBLStataLR',
    substitutions=pst.standardSubstitutions)
